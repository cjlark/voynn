from __future__ import print_function

import threading
from _tracemalloc import start
from queue import Queue
import math

import socket, pickle
from IPython import display
from itertools import *
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.contrib.predictor import predictor
from tensorflow.contrib.predictor import from_estimator
from tensorflow.estimator import Estimator
from tensorflow import keras

import time
import multiprocessing
from multiprocessing.pool import Pool
from tensorflow.python.data import Dataset
from tensorflow.python.estimator.export.export import build_parsing_serving_input_receiver_fn
from datetime import datetime

FEATURE_DIM = 19
tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format

def predict_value(optoPred, queue):
    Pred = ""
    Pred = list(p["predictions"] for p in islice(optoPred, 1))[0]
    queue.put(Pred)

# Data comes in as a dict of predicted values
def sort_outgoing_data(data):

    preds = ""
    vals = []
    for v in np.nditer(data["output"]):
        vals.append(v)
    vals[0] *= (22.659--21.096)
    vals[0] += (-21.096)

    vals[1] *= (28.12848--31.477120000000003)
    vals[1] += (-31.477120000000003)

    vals[2] *= (22.65905--39.2907)
    vals[2] += (-39.2907)

    return preds

def sort_mult_preads(dataX, dataY, dataZ):

    preds = ','.join(str(v) for v in np.nditer(dataX["output"]))
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(dataY["output"]))
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(dataZ["output"]))
    preds += ",0,0,0,0,0,0,0,0,0"

    return preds

def sort_mult_preads_rot(dataX, dataY, dataZ, deltaRot):

    preds = ','.join(str(v) for v in np.nditer(dataX["output"]))
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(dataY["output"]))
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(dataZ["output"]))
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(deltaRot["output"]))

    return preds

def sort_mult_comb(comb, deltaRot):
    preds = ""
    val = []
    for v in np.nditer(comb["output"]):
        val.append(v)
    vals = np.copy(val)
    """
    vals[0] *= (22.659 - -21.096)
    vals[0] += (-21.096)

    vals[1] *= (28.12848 - -31.477120000000003)
    vals[1] += (-31.477120000000003)

    vals[2] *= (22.65905 - -39.2907)
    vals[2] += (-39.2907)
    """
    preds += ','.join(str(v) for v in vals)
    preds += ","
    preds += ','.join(str(v) for v in np.nditer(deltaRot["output"]))

    return preds

def sort_incoming_data(d):
    #data comes in as bytes- converted to string

    nnDataHolder = pd.DataFrame({'NDI_X':[float(d[0])], 'NDI_Y':[float(d[1])], 'NDI_Z':[float(d[2])], 'NDI_Rot_X1':[float(d[3])], 'NDI_Rot_X2':[float(d[4])], 'NDI_Rot_X3':[float(d[5])], 'NDI_Rot_Y1':[float(d[6])], 'NDI_Rot_Y2':[float(d[7])],
                       'NDI_Rot_Y3': [float(d[8])],'NDI_Rot_Z1':[float(d[9])],'NDI_Rot_Z2':[float(d[10])],'NDI_Rot_Z3':[float(d[11])],'NDI_Quality':[float(d[12])],'Kuka_X':[float(d[13])],'Kuka_Y':[float(d[14])],
                       'Kuka_Z': [float(d[15])],'Kuka_A':[float(d[16])],'Kuka_B':[float(d[17])],'Kuka_C':[float(d[18])]})

    """
    nnDataHolder["NDI_X"] -= (247.352)
    nnDataHolder["NDI_X"] /= (260.189 - 247.352)

    nnDataHolder["NDI_Y"] -= (-4.12998)
    nnDataHolder["NDI_Y"] /= (2.34404 - -4.12998)

    nnDataHolder["NDI_Z"] -= (-34.8258)
    nnDataHolder["NDI_Z"] /= (-21.0964 - -34.8258)

    nnDataHolder["NDI_Rot_X1"] -= (-0.99939)
    nnDataHolder["NDI_Rot_X1"] /= (-0.988403 - -0.99939)

    nnDataHolder["NDI_Rot_X2"] -= (-0.151611)
    nnDataHolder["NDI_Rot_X2"] /= (0.075073 - -0.151611)

    nnDataHolder["NDI_Rot_X3"] -= (-0.030151)
    nnDataHolder["NDI_Rot_X3"] /= (0.052124 - -0.030151)

    nnDataHolder["NDI_Rot_Y1"] -= (-0.11499)
    nnDataHolder["NDI_Rot_Y1"] /= (0.072388 - -0.11499)

    nnDataHolder["NDI_Rot_Y2"] -= (0.683716)
    nnDataHolder["NDI_Rot_Y2"] /= (0.709106 - 0.683716)

    nnDataHolder["NDI_Rot_Y3"] -= (0.703613)
    nnDataHolder["NDI_Rot_Y3"] /= (0.726318 - 0.703613)

    nnDataHolder["NDI_Rot_Z1"] -= (-0.131348)
    nnDataHolder["NDI_Rot_Z1"] /= (0.062988 - -0.131348)

    nnDataHolder["NDI_Rot_Z2"] -= (0.70301)
    nnDataHolder["NDI_Rot_Z2"] /= (0.71887 - 0.7030)

    nnDataHolder["NDI_Rot_Z3"] -= (-0.710449)
    nnDataHolder["NDI_Rot_Z3"] /= (-0.68689 - -0.710449)

    nnDataHolder["NDI_Quality"] -= (40.0)
    nnDataHolder["NDI_Quality"] /= (1024.0 - 40.0)

    nnDataHolder["Kuka_X"] -= (563.893)
    nnDataHolder["Kuka_X"] /= (702.147 - 563.893)

    nnDataHolder["Kuka_Y"] -= (-49.143)
    nnDataHolder["Kuka_Y"] /= (76.329 - -49.143)

    nnDataHolder["Kuka_Z"] -= (23.134)
    nnDataHolder["Kuka_Z"] /= (151.919 - 23.134)

    nnDataHolder["Kuka_A"] -= (1.532)
    nnDataHolder["Kuka_A"] /= (1.639 - 1.532)

    nnDataHolder["Kuka_B"] -= (-0.624)
    nnDataHolder["Kuka_B"] /= (0.565 - -0.624)

    nnDataHolder["Kuka_C"] -= (-0.309)
    nnDataHolder["Kuka_C"] /= (0.217 - -0.309)

    print(time.time() - start)
    """
    inputnums = nnDataHolder.values
    inputnums = np.float32(inputnums)
    inputnums = inputnums.reshape(1, FEATURE_DIM)
    inputs = {"x": inputnums}
    return (inputs)


def serving_input_fn():
    x = tf.placeholder(dtype=tf.float32, shape=[None,FEATURE_DIM], name ='x')
    inputs = {'x':x}
    return tf.estimator.export.ServingInputReceiver(inputs,inputs)

def model_fn(features, labels, mode):
    nn = features["x"]
    for hiddenUnits in [10,10]:
        nn = tf.layers.dense(nn, units=hiddenUnits, activation=tf.nn.leaky_relu) #set up activation fn
        nn = tf.layers.dropout(nn, rate=0.011) #dropout randomly settings the fraction of input units to drop to avoid overfitting

    output = tf.layers.dense(nn, 1, activation=None)

    if mode==tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=output, export_outputs={"out": tf.estimator.export.PredictOutput(output)})

    loss = tf.losses.mean_squared_error(labels,output)

    if mode == tf.estimator.ModeKeys.EVAL: #sets EVAL to output the mean square error
        return tf.estimator.EstimatorSpec(mode, loss=loss)

    #Define optimizer as AdaDelta optimier (CAN CHANGE LATER)
    optimizer = tf.train.MomentumOptimizer(learning_rate=0.01,momentum=0.9)
    train = optimizer.minimize(loss,global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train)

def model_fn_rot(features, labels, mode):
    nn = features["x"]
    for hiddenUnits in [10,10]:
        nn = tf.layers.dense(nn, units=hiddenUnits, activation=tf.nn.leaky_relu) #set up activation fn
        nn = tf.layers.dropout(nn, rate=0.011) #dropout randomly settings the fraction of input units to drop to avoid overfitting

    output = tf.layers.dense(nn, 9, activation=None)

    if mode==tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=output, export_outputs={"out": tf.estimator.export.PredictOutput(output)})

    loss = tf.losses.mean_squared_error(labels,output)

    if mode == tf.estimator.ModeKeys.EVAL: #sets EVAL to output the mean square error
        return tf.estimator.EstimatorSpec(mode, loss=loss)

    #Define optimizer as AdaDelta optimier (CAN CHANGE LATER)
    optimizer = tf.train.MomentumOptimizer(learning_rate=0.5,momentum=0.9)
    train = optimizer.minimize(loss,global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train)

def model_fn_comb(features, labels, mode):
    nn = features["x"]
    for hiddenUnits in [10,10]:
        nn = tf.layers.dense(nn, units=hiddenUnits, activation=tf.nn.leaky_relu) #set up activation fn
        nn = tf.layers.dropout(nn, rate=0.011) #dropout randomly settings the fraction of input units to drop to avoid overfitting

    output = tf.layers.dense(nn, 3, activation=None)

    if mode==tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=output, export_outputs={"out": tf.estimator.export.PredictOutput(output)})

    loss = tf.losses.mean_squared_error(labels,output)

    if mode == tf.estimator.ModeKeys.EVAL: #sets EVAL to output the mean square error
        return tf.estimator.EstimatorSpec(mode, loss=loss)

    #Define optimizer as AdaDelta optimier (CAN CHANGE LATER)
    optimizer = tf.train.MomentumOptimizer(learning_rate=0.3,momentum=0.9)
    train = optimizer.minimize(loss,global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train)


startTime = time.time()
if __name__ == "__main__":
    # Declare a socket and try to connect on port 2005
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    host = "127.0.0.1"
    port = 2005

    try:
        s.bind((host, port))

    except socket.error:
        print("Binding Failed")
    s.listen(4)

    print("")
    print("Listening for client . . .")
    conn, address = s.accept()

    print("Connected to client at ", address)
    print("")
    print("Creating Predictor")

    estimatorRot = tf.estimator.Estimator(model_fn=model_fn_rot,
                                       model_dir="C:\\Users\\cjl\\Desktop\\NN_Repo\\Neural Network\\Gen2Save\\Rot3")
    estimatorComb = tf.estimator.Estimator(model_fn=model_fn_comb,
                                          model_dir="C:\\Users\\cjl\\Desktop\\NN_Repo\\Neural Network\\Gen2Save\\PosBest")


    predictorRot = from_estimator(estimatorRot, serving_input_receiver_fn=serving_input_fn)
    predictorComb = from_estimator(estimatorComb, serving_input_receiver_fn=serving_input_fn)

    #For Seeing if we need to use the secondary neural network
    #secondaryNetwork = load_model("C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Secondary Network Save\\secondaryNN.h5")

    print("Predictor created.")
    i = 0
    print("Predicting")
    while True:

        output = conn.recv(2048)
        start = time.time()

        if output: # If there is data to be received

            output = output.decode()

            output = output.replace(';','')
            if len(output) > 0:
                output = output.split(",")


                incomingData = sort_incoming_data(output)

                predictionComb = predictorComb(incomingData)
                predictionRot = predictorRot(incomingData)
                outgoingData = sort_mult_comb(predictionComb, predictionRot)

                outgoingData = outgoingData.encode()
                conn.send(outgoingData)
                #print(time.time() - start)



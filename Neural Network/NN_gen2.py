from __future__ import print_function

import math
import shutil
from IPython import display
from itertools import islice
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset
from tensorflow.contrib.predictor import predictor
from tensorflow.contrib.predictor import from_estimator
import datetime
import time
FEATURE_DIM = 19
LABEL_DIM = 9
tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format


nnDataHolder = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN_Repo\\TestResults\\t.csv", sep=",")# Filling an object with training data
dataHolderSize = len(nnDataHolder.index)
print("Num Sampels: " + str(dataHolderSize))
answers = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN_Repo\\TestResults\\t.csv", sep=",")
print(answers)
print(nnDataHolder)

#This function normalizes the input data and then saves the normalization values to a text file
def normalize_data(nnDataHolder):
    file_object = open("C:\\Users\\cjl\\Desktop\\NN_Repo\\TestResults\\normalizationData\\combL1NonStandardized" + ".txt", "w")
    file_object2 = open("C:\\Users\\cjl\\Desktop\\NN_Repo\\TestResults\\normalizationData\\bounds" + ".txt", "w")
    columnNames = list(nnDataHolder.head(0))
    """
    for i in range(0, 11):
        min = nnDataHolder[columnNames[i]].min()
        max = nnDataHolder[columnNames[i]].max()

        nnDataHolder[columnNames[i]] -= min
        nnDataHolder[columnNames[i]] /= (max - min)
        nnDataHolder[columnNames[i]] *= 2
        nnDataHolder[columnNames[i]] -= 1

        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] -= (" + str(min) + ")\n")
        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] /= (" + str(max) + "-" + str(min) + ")\n")
        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] *= (" + str(2) + ")\n")
        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] -= (" + str(1) + ")\n\n")
    """
    for i in range(12, 31):
        min = nnDataHolder[columnNames[i]].min()
        max = nnDataHolder[columnNames[i]].max()

        nnDataHolder[columnNames[i]] -= min
        nnDataHolder[columnNames[i]] /= (max - min)

        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] -= (" + str(min) + ")\n")
        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] /= (" + str(max) + "-" + str(min) + ")\n\n")
        file_object2.write("{:.5f}".format(min) + "," + "{:.5f}".format(max) + "\n");

    file_object.close()

normalize_data(nnDataHolder)
pltData = nnDataHolder
del pltData['Kuka_TSL']

nnDataHolder = nnDataHolder.reindex(np.random.permutation(nnDataHolder.index))                    # Randomizes the data
nnDataHolder = nnDataHolder.sample(frac=1)

def sort_incoming_data(d):
    #data comes in as bytes- converted to string
    inputnums = d.values
    inputnums = np.float32(inputnums)
    inputnums = inputnums.reshape(1, FEATURE_DIM)
    inputs = {"x": inputnums}
    return (inputs)

def preprocess_features(nnDataHolder):
    """Prepares input features from the nnDataHolder dataset.

    Args:
        nnDataHolder: A Pandas DataFrame which contains data from test dataset.
    Returns:
        A DataFrame that contains the features to be used for the model (including synthetic features)
    """

    selected_features = nnDataHolder[
        ["NDI_X",
         "NDI_Y",
         "NDI_Z",
         "NDI_Rot_X1",
         "NDI_Rot_X2",
         "NDI_Rot_X3",
         "NDI_Rot_Y1",
         "NDI_Rot_Y2",
         "NDI_Rot_Y3",
         "NDI_Rot_Z1",
         "NDI_Rot_Z2",
         "NDI_Rot_Z3",
         "NDI_Quality",
         "Kuka_X",
         "Kuka_Y",
         "Kuka_Z",
         "Kuka_A",
         "Kuka_B",
         "Kuka_C"]]
    processed_features = selected_features.copy()
    # Create any needed synthetic features here.
    return processed_features


def preprocess_targets(nnDataHolder):
    """Prepares target features (ie Labels/Answers).

    Args:
        nnDataHolder : Pandas DataFrame expected to contain testing data
    Returns:
        output_targets : Pandas DataFrame which contains the labels from the data
    """

    output_targets = nnDataHolder[
        [
            #"Dif_X1",
            #"Dif_X",
            #"Dif_Y",
            #"Dif_Z"
            "Dif_X1",
            "Dif_X2",
            "Dif_X3",
            "Dif_Y1",
            "Dif_Y2",
            "Dif_Y3",
            "Dif_Z1",
            "Dif_Z2",
            "Dif_Z3"
            #"Dif_Y"
        ]
    ]
    processed_targets = output_targets.copy()
    return processed_targets

# Choose 70 percent of the examples to be for training the data
training_examples = preprocess_features(nnDataHolder.head(int(dataHolderSize*0.80)))

#choose 30 percent to be for validation
validation_examples = preprocess_features(nnDataHolder.tail(int(dataHolderSize*0.20)))


training_targets = preprocess_targets(nnDataHolder.head(int(dataHolderSize*0.80)))
validation_targets = preprocess_targets(nnDataHolder.tail(int(dataHolderSize*0.20)))

plt_targets = preprocess_targets(pltData)
plt_examples = preprocess_features(pltData)
# Have one input for a test at the end
print("Training examples summary:")
display.display(training_examples.describe())


print("Training targets summary:")
display.display(training_targets.describe())

def serving_input_fn():
    x = tf.placeholder(dtype=tf.float32, shape=[None,FEATURE_DIM], name ='x')
    inputs = {'x':x}
    return tf.estimator.export.ServingInputReceiver(inputs,inputs)

def model_fn(features, labels, mode):
    nn = features["x"]
    for hiddenUnits in [10,10]:
        nn = tf.layers.dense(nn, units=hiddenUnits, activation=tf.nn.leaky_relu, ) #set up activation fn
        nn = tf.layers.dropout(nn, rate=0.011) #dropout randomly settings the fraction of input units to drop to avoid overfitting

    output = tf.layers.dense(nn, LABEL_DIM, activation=None, )

    if mode==tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=output, export_outputs={"out": tf.estimator.export.PredictOutput(output)})

    loss = tf.losses.mean_squared_error(labels,output)


    if mode == tf.estimator.ModeKeys.EVAL: #sets EVAL to output the mean square error
        return tf.estimator.EstimatorSpec(mode, loss=loss)

    #Define optimizer as Momentum optimier
    optimizer = tf.train.MomentumOptimizer(learning_rate=0.5,momentum=0.9)
    #optimizer = tf.train.AdadeltaOptimizer(learning_rate=0.01)
    train = optimizer.minimize(loss,global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train)

def my_input_function(features, targets, batch_size=1, shuffle = True, num_epochs = None):
    """This function is used to train a neural network regression model

    Args:
        features: pandas DataFrame of features
        targets: pandas DataFrame of targets
        batch_size: size of batches to be passed to the model (# of examples used in a batch)
        batch: the set of examples used in one iteration of training
        shuffle: True or False. Whether or not to shuffle the data
        num_epochs: number of epochs for which the data should be repeated. None = repeat indefinitely
    Returns:
        Tuple of (features, labels) for next data batch
    """

    # Convert Pandas data into a dict of np arrays because this is what dataset takes
    # We used a Pandas DataFrame to initially store the data and now are converting it to a dictionary -> {col1: [row1,row2,...], col2:[row1,row2..],...}
    features = features.values
    features = np.float32(features)
    targets = targets.values
    targets = np.float32(targets)

    # Construct a dataset, and configure the batching/repeating
    ds = Dataset.from_tensors(({"x":features},targets)) #ORIGINALLY FROM TENSOR SLICES

    ds = ds.batch(batch_size).repeat(num_epochs)
    #print(ds)
    # Shuffle the data, if specified.
    #if shuffle:
    #   ds = ds.shuffle(10000)

    # Return the next batch of data
    features, labels = ds.make_one_shot_iterator().get_next()

    return features, labels


if __name__ == '__main__':

    batch_size = 250
    #print("Please select desired training:")
    #print("1. Train a new model.")
    #print("2. Train an existing model.")
    #choice = input("Please enter 1 or 2")

    #if choice == "1":
    print("Training new model.")
    estimator = tf.estimator.Estimator(model_fn=model_fn,model_dir="C:\\Users\\cjl\\Desktop\\NN_Repo\\Neural Network\\Gen2Save\\estimator-predictor-test-{date:%Y-%m-%d %H.%M.%S}".format(date=datetime.datetime.now()))
    #if choice == "2":
    #   print("Training old model.")
    #    name = input("Please enter path of previously saved checkpoint.")
    #   estimator = tf.estimator.Estimator(model_fn=model_fn, model_dir=name)


    for i in range(10):
        print("Training")
        train = estimator.train(input_fn=lambda: my_input_function(training_examples, training_targets, batch_size=batch_size),steps=2000)

        print("Evaluating")
        evaluate = estimator.evaluate(lambda:my_input_function(validation_examples, validation_targets, num_epochs=1, shuffle=False), steps=1)
        print(evaluate)


    predictor = from_estimator(estimator, serving_input_receiver_fn=serving_input_fn)
    predicted_valuesX = []
    predicted_valuesY = []
    predicted_valuesZ = []
    true_valuesX = []
    true_valuesY = []
    true_valuesZ = []
    print(pltData.size)
    x = 0
    xvals = []
    for i in range(pltData.shape[0]):

        incoming_data = sort_incoming_data(plt_examples.ix[i])
        pred = predictor(incoming_data)
        prediction = ','.join(str(v) for v in np.nditer(pred["output"]))
        predictionList = [float(i) for i in prediction.split(',')]


        predX = predictionList[0]
        #predX += 1
        #predX /= 2
        #predX *= (3.683--12.166)
        #predX +=(-12.166)


        predY = predictionList[1]
        #predY += 1
        #predY /= 2
        #predY *= (7.367--7.8135)
        #predY += (-7.8135)

        predZ = predictionList[2]
        #predZ += 1
        #predZ /= 2
        #predZ *= (22.3242--31.812)
        #predZ += (-31.812)

        predicted_valuesX.append(predX)
        predicted_valuesY.append(predY)
        predicted_valuesZ.append(predZ)
        true_valuesX.append(float(answers.ix[i]['Dif_X']))
        true_valuesY.append(float(answers.ix[i]['Dif_Y']))
        true_valuesZ.append(float(answers.ix[i]['Dif_Z']))

        xvals.append(x)
        x+=1

    plt.subplot(1, 2, 2)
    plt.ylabel('X Difference')
    plt.xlabel('Sample #')
    plt.title("Predicted Value vs True Value")
    plt.tight_layout()
    plt.plot(xvals, predicted_valuesX)
    plt.plot(xvals, true_valuesX)
    plt.plot()
    plt.show()

    plt.subplot(1, 2, 2)
    plt.ylabel('Y Difference')
    plt.xlabel('Sample #')
    plt.title("Predicted Value vs True Value")
    plt.tight_layout()
    plt.plot(xvals, predicted_valuesY)
    plt.plot(xvals, true_valuesY)
    plt.plot()
    plt.show()

    plt.subplot(1, 2, 2)
    plt.ylabel('Z Difference')
    plt.xlabel('Sample #')
    plt.title("Predicted Value vs True Value")
    plt.tight_layout()
    plt.plot(xvals, predicted_valuesZ)
    plt.plot(xvals, true_valuesZ)
    plt.plot()
    plt.show()

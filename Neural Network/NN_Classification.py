from __future__ import print_function

import math
import shutil
from IPython import display
from itertools import islice
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.data import Dataset
from tensorflow.contrib.predictor import predictor
from tensorflow.contrib.predictor import from_estimator
import datetime
import time
tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format

filename = "secondaryNN"
nnDataHolder = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\" + filename + ".csv", sep=",")# Filling an object with training data
dataHolderSize = len(nnDataHolder.index)
print("Num Sampels: " + str(dataHolderSize))

def normalize_data(nnDataHolder):
    file_object = open("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\normalizationData\\" + filename + ".txt", "w")
    columnNames = list(nnDataHolder.head(0))
    for i in range(12, 31):
        min = nnDataHolder[columnNames[i]].min()
        max = nnDataHolder[columnNames[i]].max()

        nnDataHolder[columnNames[i]] -= min
        nnDataHolder[columnNames[i]] /= (max - min)

        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] -= (" + str(min) + ")\n")
        file_object.write("nnDataHolder[\"" + columnNames[i] + "\"] /= (" + str(max) + "-" + str(min) + ")\n\n")

    file_object.close()

def find_small_Diff(nnDataHolder):
    rows = len(nnDataHolder.index)
    labels = []
    for i in range(nnDataHolder.shape[0]):
        currentRow = nnDataHolder.ix[i]
        #If X, Y, and Z different are all less than 0.5mm for the current row, then append 0 else append 1
        if( (abs(currentRow['Dif_X']) < 0.5) and (abs(currentRow['Dif_Y']) < 0.5) and (abs(currentRow['Dif_Z']) < 0.5)):
            labels.append(0)
        else:
            labels.append(1)

    nnDataHolder["Activation_Labels"] = labels
    return nnDataHolder

normalize_data(nnDataHolder)
nnDataHolder = find_small_Diff(nnDataHolder)
print(nnDataHolder)
print(nnDataHolder.describe())
print(nnDataHolder.isnull().any())
nnDataHolder = nnDataHolder.reindex(np.random.permutation(nnDataHolder.index))                    # Randomizes the data
nnDataHolder = nnDataHolder.sample(frac=1)

def preprocess_features(nnDataHolder):
    """Prepares input features from the nnDataHolder dataset.

    Args:
        nnDataHolder: A Pandas DataFrame which contains data from test dataset.
    Returns:
        A DataFrame that contains the features to be used for the model (including synthetic features)
    """

    selected_features = nnDataHolder[
        ["NDI_X",
         "NDI_Y",
         "NDI_Z",
         "NDI_Rot_X1",
         "NDI_Rot_X2",
         "NDI_Rot_X3",
         "NDI_Rot_Y1",
         "NDI_Rot_Y2",
         "NDI_Rot_Y3",
         "NDI_Rot_Z1",
         "NDI_Rot_Z2",
         "NDI_Rot_Z3",
         "NDI_Quality",
         "Kuka_X",
         "Kuka_Y",
         "Kuka_Z",
         "Kuka_A",
         "Kuka_B",
         #"Kuka_C"
          ]]
    processed_features = selected_features.copy()
    # Create any needed synthetic features here.
    return processed_features

features = preprocess_features(nnDataHolder.head(int(dataHolderSize*0.70)))
labels = nnDataHolder.head(int(dataHolderSize*0.70))["Activation_Labels"]

test_features = preprocess_features(nnDataHolder.tail(int(dataHolderSize*0.300)))
test_labels = nnDataHolder.tail(int(dataHolderSize*0.30))["Activation_Labels"]
if __name__ == '__main__':
    model = keras.Sequential([
        keras.layers.Dense(18,input_dim=18,activation=tf.nn.leaky_relu),
        keras.layers.Dense(18, activation=tf.nn.leaky_relu),
        keras.layers.Dense(2, activation=tf.nn.softmax)
    ])

    model.compile(optimizer = tf.train.AdamOptimizer(learning_rate=0.01),
                  loss = 'sparse_categorical_crossentropy',
                  metrics = ['accuracy'])

    print("Training")
    model.fit(features, labels, epochs=100)
    print("Evaluating")
    test_loss, test_acc = model.evaluate(test_features, test_labels)
    print('Test accuracy:', test_acc)

    model.save("C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Secondary Network Save\\" + filename + ".h5")
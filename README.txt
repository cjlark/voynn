This Repo contains the neural network code necessary to filter NDI frames given raw NDI input as well as robot TCP.

NN_gen2.py: 			Training neural network for sensor filtration
NN_Live_Feed_Robot1.py: 	The program which receives live data, passes through NN and then sends through a socket to another program
Gen2Save:			Place to put the saved neural networks. Used for loading the neural network each time program is run